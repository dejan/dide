Contributing
============

There are many ways you can contribute to the project. PRs that fix bugs, or implement particular feature (one PR per feature please) are most welcome.
