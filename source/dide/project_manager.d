module dide.project_manager;

import std.stdio: writeln;
import std.array: replace, join;
import std.file: FileException, read;
import std.path: baseName, relativePath;
import std.string: endsWith;
import std.process: thisThreadID;
import std.conv;

import gtk.Notebook;
import gtk.FileChooserDialog;
import gtk.Widget;
import gtk.ScrolledWindow;
import gtk.Label;
import gtk.Main;
import gtk.MessageDialog;
import gtk.TextBuffer;
import gtk.TextIter;
import gtk.TextView;
import gtk.MenuItem;
import gdk.RGBA;
import gtk.ListBoxRow;

private import gsv.SourceView;
private import gsv.SourceBuffer;
private import gsv.SourceLanguage;
private import gsv.SourceLanguageManager;
private import gsv.SourceBuffer;
private import gsv.SourceStyleSchemeManager;

import vte.Terminal;

import dide.project.project;
import dide.project_window;
import dide.toolchain;
import dide.config;

/**
 Controls the project window
 */
struct ProjectManager {
  Project project; /// Project model
  Notebook filesNotebook; /// Notebook which holds the file editors
  ProjectWindow projectWindow; /// Project's window
  SourceView[int] pageToSourceView;

  /// This is the only constructor you will need
  this(Project argProject, ProjectWindow argProjectWindow, Notebook argFilesNotebook) {
    filesNotebook = argFilesNotebook;
    projectWindow = argProjectWindow;
    project = argProject;
  }

  /**
   * In some cases given file path argFileName is already part of the project, but not open
   * in DIDE's editor. Use this function when unsure whether it should be open or not.
   */
  void maybeOpen(string argFileName) {
    Target* tgtPtr = project.targetByPath(argFileName);
    if (tgtPtr) {
      // File is already part of the project, but is not open in the editor
      if (!project.isOpen(*tgtPtr)) {
        createFilePage(argFileName);
        project.openFiles ~= tgtPtr.path;
      }
    }
  } // maybeOpen() method

  /// Asks user which file to open, and adds it to the project.
  void openFile() {
    string[] buttonCaptions = ["Cancel", "Open"];
    ResponseType[] rts = [ResponseType.CANCEL, ResponseType.OK];
    FileChooserDialog fcdlg = new FileChooserDialog("DIDE: Open File...", projectWindow, FileChooserAction.OPEN, 
      buttonCaptions, rts);
    //fcdlg.setCurrentFolder(project.projectPath);
    int ret = fcdlg.run();
    string fileName = fcdlg.getFilename();
    fcdlg.hide();

    int addFileResult = project.addFile(fileName, true);
    if (addFileResult == 2) {
      maybeOpen(fileName);
    }

    if (addFileResult == 0) {
      // File has been added to the project successfully
      createFilePage(fileName);
    }
    
    if (addFileResult == 1) {
      debug writeln("Something went wrong with adding the following file to the project: ", fileName);
    }
  }

  Widget getSourceView(string fileName) {
    SourceView sourceView = new SourceView();
    sourceView.setShowLineNumbers(true);

    sourceView.setInsertSpacesInsteadOfTabs(true);
    sourceView.setTabWidth(4);
    sourceView.setHighlightCurrentLine(true);

    SourceBuffer sb = sourceView.getBuffer();

    auto manager = new SourceStyleSchemeManager();
    auto sids = manager.getSchemeIds();
    // writeln(sids);
    auto scheme = manager.getScheme("oblivion");
    sb.setStyleScheme(scheme);
    
    string code = `import std.stdio;
    void main() {
      writeln("hello");
    }`.replace("    ", "");
    sb.setText(code);

    if (fileName != "Unknown") {
      try {
        code = cast(string)read(fileName);
      } catch ( FileException fe ) { } // TODO: handle the exception properly!
    }
    
    sb.setText(code);

    ScrolledWindow scWindow = new ScrolledWindow();
    scWindow.add(sourceView);

    SourceLanguageManager slm = new SourceLanguageManager();
    SourceLanguage dLang = slm.getLanguage("d");

    if ( dLang !is null ) {
      // writefln("Setting language to D");
      sb.setLanguage(dLang);
      sb.setHighlightSyntax(true);
    }

    sourceView.modifyFont("Terminus", 10);
    sourceView.setRightMarginPosition(110);
    sourceView.setShowRightMargin(true);
    sourceView.setAutoIndent(true);
    sourceView.setShowLineMarks(true);

    return scWindow;
  }

  void compileFile() {
    projectWindow.controlNb.setCurrentPage(0); // Make the "Build" page current
    int pageNumber = filesNotebook.getCurrentPage;
    debug writeln(pageNumber);
    auto childWidget = filesNotebook.getNthPage(pageNumber);
    string tabLabel = filesNotebook.getTabLabelText(childWidget);
    debug writeln(tabLabel);
    if (tabLabel == "Unknown") {
      auto msgdlg = new MessageDialog(this.projectWindow, DialogFlags.MODAL, MessageType.WARNING, ButtonsType.OK, 
                                      "File not saved.");
      msgdlg.run();
      msgdlg.destroy(); 
      return;
    }

    Target srcTarget = project.getTargetByPageNumber(pageNumber);
    string cli = project.toolchain.compileArgs(srcTarget.path, srcTarget.destTargets[0].path).join(" ") ~ "\n";
    if (_buildTextView is null) { 
      return;
    }
    TextBuffer tbuf = _buildTextView.getBuffer();
    TextIter tite;
    tbuf.getEndIter(tite);
    
    auto objTarget = srcTarget.destTargets[0];
    int ret = project.toolchain.compile(srcTarget.path, objTarget.path, project.projectPath);
    if (ret == 0) {
      // success, CLI should be green
      tbuf.insertWithTagsByName(tite, cli, ["bold", "monospace", "fg_green"]);
    } else {
      // failure of some sort, CLI is orange
      tbuf.insertWithTagsByName(tite, cli, ["bold", "monospace", "fg_orange"]);
    }

    if (project.toolchain.processOutput !is null) {
      tbuf.insertWithTagsByName(tite, project.toolchain.processOutput, ["monospace"]);
    }

    // Append process exit code too
    tbuf.insert(tite, to!string(ret) ~ "\n");

    /+ Is there an executable target as destination target inside the objTarget? If so we will run the
     | linking process as well, to produce the executable.
     +/
    if (objTarget.targetType == TargetType.Object 
        && objTarget.destTargets !is null 
        && objTarget.destTargets.length > 0) {
      auto exeTarget = objTarget.destTargets[0];
      cli = project.toolchain.linkArgs(objTarget.path, exeTarget.path).join(" ") ~ "\n";
      ret = project.toolchain.link(objTarget.path, exeTarget.path, project.projectPath);
      if (ret == 0) {
        // success, CLI should be green
        tbuf.insertWithTagsByName(tite, cli, ["bold", "monospace", "fg_green"]);

        // Let's add newly created executable target to the TLT list box:
        string tltName = exeTarget.path.replace(project.projectPath, "*");
        tltName = tltName.replace("_", "__"); // we have to replace _ with __ so it is shown properly
        Label newLabel = new Label(tltName);
        auto greenColor = new RGBA(0.0, 0.7, 0.0, 1.0);
		    newLabel.overrideColor(StateFlags.NORMAL, greenColor);
        newLabel.setHalign(GtkAlign.END);
        projectWindow.tltListBox.add(newLabel);
        projectWindow.tltListBox.showAll();
      } else {
        // failure of some sort, CLI is orange
        tbuf.insertWithTagsByName(tite, cli, ["bold", "monospace", "fg_orange"]);
      } 
      if (project.toolchain.processOutput !is null) {
        tbuf.insertWithTagsByName(tite, project.toolchain.processOutput, ["monospace"]);
      }
      // Append process exit code too
      tbuf.insert(tite, to!string(ret) ~ "\n");
    }
  } // compileFile()

  void handleQuit(bool skipXY=false) {
    debug writeln("handleQuit(", skipXY, ")");
    projectWindow.updateUserConfig(skipXY);
    debug writeln(project.userConfig);
    debug writeln(projectWindow.projectNb.getCurrentPage);
    debug writeln(projectWindow.controlNb.getCurrentPage);
    saveUserConfig(project.userConfig, project.openFiles, project.name);
    
    import dide.project.project_data_file;
    ProjectDataFile projdf = new ProjectDataFile(project);
    projdf.save();

    Main.quit();
  }

  void runFile() {
    int pageNumber = filesNotebook.getCurrentPage;
    auto childWidget = filesNotebook.getNthPage(pageNumber);
    string tabLabel = filesNotebook.getTabLabelText(childWidget);
    if (tabLabel == "Unknown") {
      auto msgdlg = new MessageDialog(this.projectWindow, DialogFlags.MODAL, MessageType.WARNING, ButtonsType.OK, 
                                      "File not saved.");
      msgdlg.run();
      msgdlg.destroy(); 
      return;
    }
    // Finally, we have the source target...
    Target srcTarget = project.getTargetByPageNumber(pageNumber);

    Terminal terminal = new Terminal();

    const int newPageNumber = projectWindow.controlNb.appendPage(terminal, new Label("Run file"));
    projectWindow.controlNb.showAll();
    // needs to be called after showAll() ... sigh
    projectWindow.controlNb.setCurrentPage(newPageNumber);
    terminal.grabFocus();

    // read -n 1 -s -r -p "Press any key to continue"
    string[] args = ["/usr/bin/env",  "bash", "-c", project.toolchain.scrunner ~ " " ~ srcTarget.path];
    debug writeln(args);
    terminal.spawnAsync(PtyFlags.DEFAULT, project.projectPath, args, 
                        null, SpawnFlags.DEFAULT, null, null, null, 3600, null, null, null);
    terminal.addOnChildExited((int ii, Terminal tt) {
      // only when child exited we add the OnKeyPress handler, so that we can close the page
      // when user presses any key
      tt.addOnKeyPress((GdkEventKey* kk, Widget ww) {
        debug writeln("key pressed");
        projectWindow.controlNb.removePage(newPageNumber);
        return true;
      });
    });
  } // runFile() method

  void createFilePage(string fileName) {
    string baseFileName = baseName(fileName);
    
    // Issue: dejan/dide#15
    // We have to replace one underscore with two because GTK treats single one as start of mnemonic.
    baseFileName = baseFileName.replace("_", "__");

    project.setActiveSourceTarget(fileName);
    const int pageNumber = filesNotebook.appendPage(getSourceView(fileName), new Label(baseFileName));
    // We need to "inform" the project object to associate newly created page number with newly created source target.
    project.setPageNumber(pageNumber);
    filesNotebook.showAll();
    // Has to be called after showAll() , otherwise it will not switch to the new page
    filesNotebook.setCurrentPage(pageNumber);
    Target srcTarget = project.getTargetByPageNumber(filesNotebook.getCurrentPage);
  }

  void handleClose() {
    int pageNumber = filesNotebook.getCurrentPage();
    project.closeFile(pageNumber);
    filesNotebook.removePage(pageNumber);
  }

  void projectAddFileHandler() {
    debug writeln("projectAddFileHandler()");
    int pageNumber = filesNotebook.getCurrentPage();
    
    bool added = project.addFileToProject(pageNumber);
    if (added) {
      Target target = project.getTargetByPageNumber(pageNumber);

      // Issue: dejan/dide#15
      // We have to replace one underscore with two because GTK treats single one as start of mnemonic.
      string caption = project.shortPath(target).replace("_", "__");
      Label tmpLabel = new Label(caption);

      // We need to set label's "name" so that we can identify it later
      tmpLabel.setName(target.path);
      // we also set user-data with key "target" to point to the Target itself
      tmpLabel.setData("target", cast(void*)&target);

      tmpLabel.setHalign(Align.END);
      projectWindow.srcListBox.add(tmpLabel);
      projectWindow.srcListBox.showAll();
    }
  }

  void projectRemoveFileHandler() {
    debug writeln("projectRemoveFileHandler()");
    // TODO:
    int pageNumber = filesNotebook.getCurrentPage();
    Target tgt = project.getTargetByPageNumber(pageNumber);
    debug writeln(tgt.path);
    if (tgt.projectTarget) {
      bool removed = project.removeFileByPath(tgt.path);
      if (removed) {
        ListBoxRow[] lbrows = projectWindow.srcListBox.getChildren().toArray!ListBoxRow; 
        foreach (lbr; lbrows) {
          Widget lbl = lbr.getChild();
          string path = lbl.getName();
          writeln(path);
          if (path == tgt.path) {
            projectWindow.srcListBox.remove(lbr);
          }
        }
      }
    }
  } // projectRemoveFileHandler()

  /**
   Although AboutDialog is really convenient, at some point in the future 
   I will implement a simpler one that suits my needs. This will do for now...
   */
  void handleAbout(MenuItem argMenuItem) {
    import gtk.AboutDialog: AboutDialog;
    import gdk.Pixbuf: Pixbuf;
    import gdkpixbuf.PixbufLoader: PixbufLoader;
    import dide.res: DIDEW320_PNG;

    // TODO: maybe a good idea is to make this happen during the initialisation phase instead of
    // doing it here whenever user clicks on About menu item, which, luckilly, is not frequent.
    PixbufLoader pl = new PixbufLoader();
    pl.write(cast(char[])DIDEW320_PNG);
    pl.close();
    
    AboutDialog aboutDialog = new AboutDialog();
    aboutDialog.setAuthors(["Dejan Lekić - dejan.lekic(at)google.com"]);
    // aboutDialog.addCreditSection(sectionName, people); // shows when the Credits button is clicked
    aboutDialog.setCopyright("Copyright (c) 2021-, Dejan Lekić");
    string comments = "Special thanks to:\n"
      ~ "Mike Way for his work on GtkD\n"
      ~ "Ron Tarrant for his gtkDCoding website.";
    aboutDialog.setComments(comments);
    aboutDialog.setLicense("BSD 3-Clause License");
    aboutDialog.setProgramName("DIDE v0.1.0");
    aboutDialog.setLogo(pl.getPixbuf());
    aboutDialog.setTransientFor(projectWindow);
    // Does not exist yet... I do own dlang.uk so should not take too long to make it happen.
    aboutDialog.setWebsite("https://dide.dlang.uk");
    aboutDialog.show();
  }

  void handleDetectTC() {
    debug writeln("handleDetectTC()");
    Toolchain dmdtc = detectDMD();
    debug writeln(dmdtc);
  }
  
  @property TextView buildTextView() { return _buildTextView; }
  @property void buildTextView(TextView arg) { _buildTextView = arg; }

  /+--------------------------------- PRIVATE STUFF ------------------------------------------+/
  private:
  TextView _buildTextView;
}
