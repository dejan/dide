module dide.utils;

import std.json;
import std.file;
import std.stdio, std.algorithm, std.array, std.traits, std.range;
import std.datetime.systime : SysTime, Clock;
import std.datetime.date : DateTime;


string[] dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

/**
 Reads given file and returns JSONValue of parsed JSON content.

 Params:
  fileName = string Name of the file we want to read.

 Returns:
  JSONValue representing parsed JSON code.
*/
JSONValue loadJSON(in string fileName) {
  string jsonBody = readText(fileName);
  return parseJSON(jsonBody);
}

/** 
 Allows something like:
 ```
 jsonval.builder = q{
    {"foo": {"bar": {"baz": 42}}, "Chuck": "Norris"}
 };
 ```
 Outer braces are a must, as that is how phobos JSON parser works.
 */
void builder(ref JSONValue value, string str) { value = parseJSON(str); }


/** 
 * A tiny utility function that builds a string in form "Wed 2022-Aug-18 21:40"
 * that we use to go to the bottom-right corner.
 */
string dateTimeString() {
    SysTime currentTime = Clock.currTime();
    DateTime dt = cast(DateTime)currentTime;
    string dtString = dayNames[currentTime.dayOfWeek] ~ " " ~ dt.toSimpleString;
    return dtString;
}

enum AreSortableArrayItems(T) = isMutable!T &&
                                __traits(compiles, T.init < T.init) &&
                                !isNarrowString!(T[]);
/** 
 Selection sort implementation found at https://rosettacode.org/wiki/Sorting_algorithms/Selection_sort#D
 */
void selectionSort(T)(T[] data) if (AreSortableArrayItems!T) {
    foreach (immutable i, ref d; data)
        data.drop(i).minPos[0].swap(d);
} unittest {
    int[] a0;
    a0.selectionSort;
 
    auto a1 = [1];
    a1.selectionSort;
    assert(a1 == [1]);
 
    auto a2 = ["a", "b"];
    a2.selectionSort;
    assert(a2 == ["a", "b"]);
 
    auto a3 = ["b", "a"];
    a3.selectionSort;
    assert(a3 == ["a", "b"]);
 
    auto a4 = ['a', 'b'];
    static assert(!__traits(compiles, a4.selectionSort));
 
    const dchar[] a5 = ['b', 'a'];
    a5.selectionSort;
    assert(a5 == "ab"d);
 
    import std.typecons: Nullable;
    alias N = Nullable!int;
    auto a6 = [N(2), N(1)];
    a6.selectionSort; // Not nothrow.
    assert(a6 == [N(1), N(2)]);
 
    auto a7 = [1.0+0i, 2.0+0i]; // To be deprecated.
    static assert(!__traits(compiles, a7.selectionSort));
 
    import std.complex: complex;
    auto a8 = [complex(1), complex(2)];
    static assert(!__traits(compiles, a8.selectionSort));
 
    static struct F {
        int x;
        int opCmp(F f) const { // Not pure.
            return x < f.x ? -1 : (x > f.x ? 1 : 0);
        }
    }
    auto a9 = [F(2), F(1)];
    a9.selectionSort;
    assert(a9 == [F(1), F(2)]);
}
