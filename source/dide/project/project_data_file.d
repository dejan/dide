module dide.project.project_data_file;

import std.json;
import std.file: write, exists;
import std.stdio: writeln;

import dide.project.project;
import dide.utils;

enum projectDataFileName = "dide.json";

/** 
 * This class contains all necessary data and operation on that data, needed to manipulate
 * information stored in the project data file, which is located inside the project file tree.
 * This file always has the same name - dide.json.
  */
final class ProjectDataFile {
  Project _project;
  
  public: /+-------------------- public stuff -----------------------------------------------+/

  this(Project project) {
    _project = project;
  }

  /** 
   * Use this method to save project data into the project data file (...path/to/project/dide.json)
   */
  void save() {
    debug writeln("save()");
    JSONValue projectJson;
    string jsonFileName = _project.projectPath ~ "/dide.json";
    debug writeln(jsonFileName);
    projectJson["name"] = _project.name;
    projectJson["type"] = _project.projectType;
    projectJson["files"] = new JSONValue[0];
    foreach (ref target; _project.sourceTargets) {
      debug writeln(_project.shortPath(target));
      if (target.projectTarget) {
        projectJson["files"].array ~= JSONValue(target.path);
      }
    }
    // TODO: maybe we should use appender here
    write(jsonFileName, projectJson.toPrettyString(JSONOptions.doNotEscapeSlashes) ~ "\n");
  } // save() method

  /**
   Use this method to load project data file content.
   */
  void load() {
    debug writeln("project_data_file.load()");
    string jsonFileName = _project.projectPath ~ "/dide.json";
    debug writeln(jsonFileName);

    string[] files;
    if (exists(jsonFileName)) {
      JSONValue json = loadJSON(jsonFileName);
      if ("files" in json) {
        foreach (jsonval; json["files"].array) {
          files ~= jsonval.str;
        }
      }
    }
    
    if (files.length > 0) {
      _project.loadProjectFiles(files);
    }
  } // load() method

} // ProjectDataFile class
