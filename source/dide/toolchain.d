module dide.toolchain;

import std.process;
import std.stdio: writeln;

import dide.project.project;

/**
 Object of this type contains all data we need to successfully use particular Toolchain.class MyClass
 */
class Toolchain {

  this(string argName) {
    _name = argName;
  }

  /**
   Build command line arguments array to be used for invoking the compiler.
   */
  string[] compileArgs(string sourceFile, string destFile, bool release = false) {
    string buildTypeFlag = "-g"; /// debug by default
    if (release) {
        buildTypeFlag = "-release";
    }
    return [compiler, "-c", buildTypeFlag, sourceFile, "-of=" ~ destFile];
  }

  string[] linkArgs(string objectFile, string destFile, bool release = false) {
    /* order rule of thumb - the most dependent files first
    0 - the output file (-o <file name>)
    1 - object files (obja.o objb.o ...)
    2 - static libraries (.a) - lphobos2 is a must for D applications
    3 - shared libraries (.so) in such order that if foo.so depends on bar.so then foo comes first
        ie. -lfoo -lbar
    */
    return [linker, "-o", destFile, objectFile, "-lphobos2"];
  }

  /** 
   Invoke compiler and store the output internally.class MyClass
   */
  int compile(string sourceFile, string destFile, string workDir = "/tmp", bool release = false) {
    auto args = compileArgs(sourceFile, destFile, release);
    debug writeln(args);
    auto ret = execute(args, null, Config.none, size_t.max, workDir);
    _process_output = ret.output;
    return ret.status;
  }

  int link(string objectFile, string destFile, string workDir = "/tmp") {
    auto args = linkArgs(objectFile, destFile);
    debug writeln(args);
    auto ret = execute(args, null, Config.none, size_t.max, workDir);
    _process_output = ret.output;
    return ret.status;
  }

  string[] scrunArgs(string sourceFile) {
    return [scrunner, sourceFile];
  }

  int scrun(string sourceFile, string workDirName = "/tmp") {
    auto args = scrunArgs(sourceFile);
    debug writeln(args);
    auto ret = execute(args, null, Config.none, size_t.max, workDirName);
    _process_output = ret.output;
    return ret.status;
  }

  override string toString() {
    // YAML-like content
    return `{"` ~ _name ~ `": {` ~
           `"compiler": "` ~ this.compiler ~ `", ` ~
           `"linker": "` ~ this.linker ~ `", ` ~
           `"dub": "` ~ this.dub ~ `", ` ~
           `"scrunner": "` ~ this.scrunner ~ `"` ~
           `}}`;
  }

  @property bool defaultToolchain() { return _defaultToolchain; }
  @property void defaultToolchain(bool arg) { _defaultToolchain = arg; }
  @property string compiler() { return _compiler; }
  @property void compiler(string arg) { _compiler = arg; }
  @property string linker() { return _linker; }
  @property void linker(string arg) { _linker = arg; }
  @property string dub() { return _dub; }
  @property void dub(string arg) { _dub = arg; }
  @property string scrunner() { return _scrunner; }
  @property void scrunner(string arg) { _scrunner = arg; }
  @property string processOutput() { return _process_output; }

  private:

  string _name; /// Human-friendly name
  bool _defaultToolchain = false; /// Is it default toolchain (true) or not (false)
  string _compiler = "dmd"; /// Either name of the compiler executable (gcc) or full path to it (/usr/bin/gcc)
  string _linker = "gcc"; /// Either name of the linker executable (gcc) or full path to it (/usr/bin/gcc)
  string _dub = "dub";  /// Either name of the DUB executable (dub) or full path to it (/usr/bin/dub)
  string _scrunner = "rdmd"; /// Either name of the script runner (rdmd) or full path to it (/usr/bin/rdmd)
  string _process_output;
} // Toolchain class


/**
 * Relatively simple function that detects DMD compiler, and the rest of the tools and returns
 * the "system-dmd" toolchain.
 */
Toolchain detectDMD() {
  import dide.various.shutil: which;

  Toolchain tc = new Toolchain("system-dmd");
  auto dmdPath = which("dmd");
  tc.compiler = (dmdPath is null) ? "/usr/bin/dmd" : dmdPath;
  auto linkerPath = which("gcc");
  tc.linker = (linkerPath is null) ? "/usr/bin/gcc" : linkerPath;
  auto dubPath = which("dub");
  tc.dub = (dubPath is null) ? "/usr/bin/dub" : dubPath;
  auto scrunnerPath = which("rdmd");
  tc.scrunner = (scrunnerPath is null) ? "/usr/bin/rdmd" : scrunnerPath;

  return tc;
} // detectDMD() function
