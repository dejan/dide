GTK3 to GTK4 Migration Guide
============================

Some help found here: https://docs.gtk.org/gtk4/migrating-3to4.html

gtk.Vbox
--------

Replace with gtk.Box with `vexpand = true`.


gtk.Table
---------

Replace with gtk.Grid.

gtk.ButtonBox
-------------

GtkButtonBox has been removed. Use a GtkBox instead.

Dialog
------

There is no dialog.run() (which returns a result) anymore. We need to set the dialog to modal, and
connect to the result signal...
https://docs.gtk.org/gtk4/migrating-3to4.html#stop-using-blocking-dialog-functions

Gdk.Color
---------

Use Gdk.RGBA instead.

MenuItem
--------

MenuBar, Menu and MenuItem are completely gone in GTK4.

Pixbuf
------

Replaced with Texture, Icon or Paintable.
